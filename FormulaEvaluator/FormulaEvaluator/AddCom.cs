﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaEvaluator {
    class AddCom : Command {

        public AddCom() {

            requiredOperandNumber = 2;
        }

        public override double Evaluate(Dictionary<string, double> parameters) {

            return operands[0].Evaluate(parameters) + operands[1].Evaluate(parameters);
        }
    }
}
