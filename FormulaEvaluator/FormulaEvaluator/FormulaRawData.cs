﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaEvaluator {
    class FormulaRawData {

        public string FormulaText;
        public string FormulaParams;
        public int FormulaId;
        public int? LastParametersId;

        public FormulaRawData(string formulaText, string formulaParams, int formulaId, int? lastParametersId) {
            FormulaText = formulaText;
            FormulaParams = formulaParams;
            FormulaId = formulaId;
            LastParametersId = lastParametersId;
        }
    }
}
