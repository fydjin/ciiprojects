﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace FormulaEvaluator {
    
    public partial class MainWindow : Window {

        public event Action<string> OnAddFormulaButtonClick;
        public event Action<string> OnFormulaSelect;
        public event Action<Dictionary<string, double>, string> OnResultButtonClick;

        private List<Parameter> parameters = new List<Parameter>();
        private Dictionary<Errors, string> errorMessages = new Dictionary<Errors, string>(); 
        
        public MainWindow() {
            InitializeComponent();
            InitializeErrorMessages();
            new Controller(this);
            DataContext = this;
        }

        private void InitializeErrorMessages() {
            errorMessages.Add(Errors.ErrorConnection, "Не удалось подключиться к базе");
            errorMessages.Add(Errors.FormulaSyntaxError, "Некорректный вид формулы");
            errorMessages.Add(Errors.FormulaAlreadyExists, "Формула уже добавлена");
            errorMessages.Add(Errors.InvalidParameterValue, "Некорректные параметры");
        }

        public void ShowError(Errors error) {
            MessageBox.Show(errorMessages[error]);
        }

        public void ShowResult(string result) {
            txtResult.Text = result;
        }

        public void FillFormules(List<Formula> formules) {

            foreach (var formula in formules) {
                ListBoxWithFormules.Items.Add(formula.FormulaText);
            }
        }

        public void AddFormula(string formula) {
            if (!String.IsNullOrEmpty(formula)) {
                ListBoxWithFormules.Items.Add(formula);
            }
        }

        private void OnAddButtonClick(object sender, RoutedEventArgs e) {
            string formula = TextBoxForEntryFormules.Text;
            OnAddFormulaButtonClick?.Invoke(formula);
        }

        private void AddParametersOnStackPanel(List<Parameter> parameters) {
            foreach (var parameter in parameters) {
                ParametersPanel.Children.Add(parameter.ParameterLabel);
                ParametersPanel.Children.Add(parameter.LastParameter);
            }
        }

        public void ShowParameters(Dictionary<string, double> variableValue) {

            foreach (var kvp in variableValue) {
                Parameter parameter = new Parameter(kvp.Key, kvp.Value.ToString(), kvp.Key);
                parameters.Add(parameter);
            }
            AddParametersOnStackPanel(parameters);
        }

        public void SelectFormula(string formulaText) {

            ListBoxWithFormules.SelectedItem = formulaText;
        }

        private void SelectedFormulaOnList(object sender, SelectionChangedEventArgs e) {
            
            object item = ListBoxWithFormules.SelectedItem;
            parameters.Clear();
            if (ParametersPanel.Children.Count > 0) {
                ParametersPanel.Children.Clear();
            }
            OnFormulaSelect?.Invoke(item.ToString());
        }

        private void OnGetResultButtonClick(object sender, RoutedEventArgs e) {

            Dictionary<string, double> variableValue = new Dictionary<string, double>();
            object item = ListBoxWithFormules.SelectedItem;

            foreach (var parameter in parameters) {
                if (Double.TryParse(parameter.LastParameter.Text, out var parametrValue)) {
                    variableValue.Add(parameter.ParameterLabel.Content.ToString(), parametrValue);
                } else {
                    ShowError(Errors.InvalidParameterValue);
                    return;
                }
            }
            OnResultButtonClick?.Invoke(variableValue, item.ToString());
        }
    }
}
