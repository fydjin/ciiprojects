﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaEvaluator {
    class OneElemCom : Command {

        private string name;
        private double value;

        private OneElemCom() {
            requiredOperandNumber = 1;
        }

        public OneElemCom(string name) : this() {
            this.name = name;
        }

        public OneElemCom(double value) : this() {
            this.value = value;
        }

        public override double Evaluate(Dictionary<string, double> parameters) {

            return name != null ? parameters[name] : value;
        }
    }
}
