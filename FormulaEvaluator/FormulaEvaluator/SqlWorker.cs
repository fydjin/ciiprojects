﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;

namespace FormulaEvaluator {
    class SqlWorker {

        private static string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        private const string queryCreateTables = @"CREATE TABLE FormulaLastParameters(ParametersId INT PRIMARY KEY IDENTITY(1, 1),LastParameters VARCHAR(50) NOT NULL);
                CREATE TABLE FormulaEvaluator(FormulaId INT PRIMARY KEY IDENTITY(1, 1),FormulaText VARCHAR(50) NOT NULL, ParametersId INT ,
                FOREIGN KEY(ParametersId) REFERENCES FormulaLastParameters(ParametersId)) ";
        private const string queryCheckFormulaEvaluatorTable = @" SELECT CASE WHEN OBJECT_ID('dbo.FormulaEvaluator', 'U') IS NOT NULL THEN 1 ELSE 0 END";
        private const string queryCheckFormulaLastParametersTable = @" SELECT CASE WHEN OBJECT_ID('dbo.FormulaLastParameters', 'U') IS NOT NULL THEN 1 ELSE 0 END";
        private const string deleteTableEvaluator = @"DROP TABLE dbo.FormulaEvaluator";
        private const string deleteTableParameters = @"DROP TABLE dbo.FormulaLastParameters";
        private const string insertFormula = "INSERT INTO FormulaEvaluatorDb.dbo.FormulaEvaluator(FormulaText) OUTPUT inserted.FormulaId VALUES(@param1)";
        private const string getFormules = @"SELECT FormulaText, FormulaId, t1.ParametersId, LastParameters from [FormulaEvaluatorDb].[dbo].[FormulaEvaluator] AS t1
	            LEFT JOIN [FormulaEvaluatorDb].[dbo].[FormulaLastParameters] AS t2 ON t1.ParametersId = t2.ParametersId";
        private const string updateFormula = @"UPDATE FormulaEvaluator SET FormulaText = @newFormulaText, ParametersId = @parametersId 
                WHERE FormulaId = @formulaId";
        private const string updateParameters = @"UPDATE FormulaLastParameters SET LastParameters = @newLastParameters 
                WHERE ParametersId = @parametersId";
        private const string createParameters = @"INSERT INTO FormulaLastParameters OUTPUT inserted.ParametersId VALUES(@newLastParameters);";

        private SqlConnection connection = new SqlConnection(connectionString);

        public List<FormulaRawData> GetFormules() {
            SqlCommand command = new SqlCommand(getFormules, connection);

            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            List<FormulaRawData> formules = new List<FormulaRawData>();
            using (reader) {

                while (reader.Read()) {
                    FormulaRawData formulaRawData = new FormulaRawData(reader["FormulaText"].ToString(),
                        reader["LastParameters"].ToString(),
                        Convert.ToInt32(reader["FormulaId"]),
                        TryConvertStringToInt(reader["ParametersId"].ToString()));
                    formules.Add(formulaRawData);
                }
            }
            connection.Close();
            return formules;
        }

        private int? TryConvertStringToInt(string str) {

            if (!string.IsNullOrEmpty(str)) {
                return Convert.ToInt32(str);
            }
            return null;
        }

        public int CreateParameters(string lastParameters) {

            SqlCommand coommand = new SqlCommand(createParameters, connection);
            int paramsId = 0;
            try {
                connection.Open();
                coommand.Parameters.AddWithValue("@newLastParameters", lastParameters);
                paramsId = (int)coommand.ExecuteScalar();

            } catch (System.Exception ex) {

            } finally {
                connection.Close();
            }
            return paramsId;
        }

        public void UpdateParameters(int? parametersId, string newLastParameters) {

            SqlCommand command = new SqlCommand(updateParameters, connection);
            try {
                connection.Open();
                command.Parameters.AddWithValue("@parametersId", parametersId);
                command.Parameters.AddWithValue("@newLastParameters", newLastParameters);
                command.ExecuteNonQuery();

            } catch (System.Exception ex) {

            } finally {
                connection.Close();
            }
        }

        public void UpdateFormula(int formulaId, string formula, int? parametersId) {

            SqlCommand command = new SqlCommand(updateFormula, connection);
            try {
                connection.Open();
                command.Parameters.AddWithValue("@formulaId", formulaId);
                command.Parameters.AddWithValue("@parametersId", parametersId);
                command.Parameters.AddWithValue("@newFormulaText", formula);
                command.ExecuteNonQuery();

            } catch (System.Exception ex) {

            } finally {
                connection.Close();
            }
        }

        public int WriteFormula(string text) {

            SqlCommand command = new SqlCommand(insertFormula, connection);
            int formulaId = 0;
            try {
                connection.Open();
                command.Parameters.AddWithValue("@param1", text);
                formulaId = (int)command.ExecuteScalar();

            } catch (System.Exception ex) {
               
            } finally {
                connection.Close();
            }
            return formulaId;
        }

        public void DropTables() {

            SqlCommand CommDeleteTableEvaluator = new SqlCommand(deleteTableEvaluator, connection);
            SqlCommand CommDeleteTableParameters = new SqlCommand(deleteTableParameters, connection);

            try {
                connection.Open();
                CommDeleteTableEvaluator.ExecuteNonQuery();
                CommDeleteTableParameters.ExecuteNonQuery();
            } catch (System.Exception ex) {

            } finally {
                connection.Close();
            }
        }

        public void CreateTable() {

            SqlCommand command = new SqlCommand(queryCreateTables, connection);
            try {
                connection.Open();
                command.ExecuteNonQuery();
            } catch (System.Exception ex) {

            } finally {
                connection.Close();
            }
        }

        public bool CheckConnection() {

            try {
                connection.Open();
                return true;
            } catch (Exception) {
                return false;
            } finally {
                connection.Close();
            }
        }

        public bool CheckTables() {

            SqlCommand CommToCheckFormulaEvaluator = new SqlCommand(queryCheckFormulaEvaluatorTable, connection);
            SqlCommand CommToCheckFormulaLastParameters = new SqlCommand(queryCheckFormulaLastParametersTable, connection);
            try {
                connection.Open();
                if ((int)CommToCheckFormulaEvaluator.ExecuteScalar() == 1 && (int)CommToCheckFormulaLastParameters.ExecuteScalar() == 1) {
                    return true;
                }
            } catch (System.Exception ex) {

            } finally {
                connection.Close();
            }
            return false;
        }
    }
}
