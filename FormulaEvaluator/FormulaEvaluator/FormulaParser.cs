﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace FormulaEvaluator {
    class FormulaParser {

        public static Formula GetFormula(FormulaRawData formulaRawData) {

            string reversText = GetExpression(formulaRawData.FormulaText);
            Formula formula = new Formula(formulaRawData.FormulaText, formulaRawData.FormulaId, formulaRawData.LastParametersId, GetCommand(reversText));
            return formula;
        }

        public static string GetExpression(string formulaText) {
            string output = string.Empty;
            Stack<char> stack = new Stack<char>();

            for (int i = 0; i < formulaText.Length; i++) {
                if (IsDelimeter(formulaText[i])) {
                    continue;
                }
                if (Char.IsLetter(formulaText[i])) {
                    while (!IsDelimeter(formulaText[i]) && !IsOperator(formulaText[i])) {
                        output += formulaText[i];
                        i++;

                        if (i == formulaText.Length) {
                            break;
                        }
                    }
                    output += " ";
                    i--;
                }
                if (Char.IsDigit(formulaText[i])) {
                    while (!IsDelimeter(formulaText[i]) && !IsOperator(formulaText[i])) {
                        output += formulaText[i];
                        i++;

                        if (i == formulaText.Length) {
                            break;
                        }
                    }
                    output += " ";
                    i--;
                }
                if (IsOperator(formulaText[i])) {
                    if (formulaText[i] == '(') {
                        stack.Push(formulaText[i]);
                    } else if (formulaText[i] == ')') {
                        char s = stack.Pop();

                        while (s != '(') {
                            output += s.ToString() + ' ';
                            s = stack.Pop();
                        }
                    } else {
                        if (stack.Count > 0)
                            if (GetPriority(formulaText[i]) <= GetPriority(stack.Peek()))
                                output += stack.Pop().ToString() + " ";

                        stack.Push(char.Parse(formulaText[i].ToString()));
                    }
                }
            }
            while (stack.Count > 0)
                output += stack.Pop() + " ";
            return output;
        }

        public static Command GetCommand(string reverseText) {

            Command command = null;
            Stack<Command> stack = new Stack<Command>();
            for (int i = reverseText.Length - 1; i >= 0; i--) {

                if (reverseText[i] == ' ') {
                    i--;
                }
                if (stack.Count == 0) {
                    command = CreateComm(reverseText[i].ToString());
                    stack.Push(command);
                } else {
                    if (stack.Peek().IsOperandsFull()) {
                        stack.Pop();
                    }
                    if (IsOperator(reverseText[i])) {
                        Command com = CreateComm(reverseText[i].ToString());
                        stack.Peek().AddOperand(com);
                        stack.Push(com);
                    } else {
                        if (Char.IsDigit(reverseText[i]) || reverseText[i] == ',') {
                            string value = string.Empty;
                            while (i >= 0 && Char.IsDigit(reverseText[i]) || reverseText[i] == ',') {
                                value = reverseText[i] + value;
                                if (i != 0) {
                                    i--;
                                } else {
                                    break;
                                }
                            }
                            stack.Peek().AddOperand(new OneElemCom(Double.Parse(value)));
                        } else {
                            string str = string.Empty;
                            while (i >= 0 && Char.IsLetter(reverseText[i])) {
                                str = reverseText[i] + str;
                                if (i != 0) {
                                    i--;
                                } else {
                                    break;
                                }
                            }
                            stack.Peek().AddOperand(new OneElemCom(str));
                        }
                    }
                }
            }
            return command;
        }

        private static Command CreateComm(string operation) {

            switch (operation) {
                case "+":
                    return new AddCom();
                case "-":
                    return new SubCom();
                case "*":
                    return new MultCom();
                case "/":
                    return new DivCom();
            }
            return null;
        }

        public static HashSet<string> ParseParameters(string formulaText) {

            HashSet<string> parameters = new HashSet<string>();
            for (int i = 0; i < formulaText.Length; i++) {
                string parameter = string.Empty;
                if (Char.IsLetter(formulaText[i])) {
                    while (!IsDelimeter(formulaText[i]) && !IsOperator(formulaText[i])) {
                        parameter += formulaText[i];
                        i++;

                        if (i == formulaText.Length) {
                            break;
                        }
                    }
                    i--;
                    parameters.Add(parameter);
                }
            }
            return parameters;
        }

        private static string AddMultiplications(string formulaText) {

            for (int i = 0; i < formulaText.Length; i++) {
                if (!IsCharIndexCorrect(i + 1, formulaText)) break;

                if (formulaText[0] == '-') {
                    formulaText = formulaText.Insert(0, "0");
                }
                if (formulaText[i] == '(' && formulaText[i + 1] == '-') {
                    formulaText = formulaText.Insert(i + 1, "0");
                }
                if (!IsOperator(formulaText[i]) && formulaText[i + 1] == '(') {

                    formulaText = formulaText.Replace($"{formulaText[i]}(", $"{formulaText[i]}*(");
                }
            }
            return formulaText;
        }

        public static bool CheckInputText(string inputText, out string formulaText) {

            string textWithOutSpaces = RemoveSpaces(inputText);
            if (!HasUnacceptableSymbols(textWithOutSpaces) &&
                IsMathOperationCorrect(textWithOutSpaces) &&
                IsBracketsCorrect(textWithOutSpaces)) {

                formulaText = AddMultiplications(textWithOutSpaces);
                return true;
            }
            formulaText = null;
            return false;
        }

        private static bool IsBracketsCorrect(string text) {

            int count = 0;
            for (int i = 0; i < text.Length; i++) {
                char symbol = text[i];
                if (symbol == '(') {
                    count++;
                } else if (symbol == ')') {
                    count--;
                }
                if (count < 0) return false;
            }
            return count == 0;
        }

        private static bool IsMathOperationCorrect(string text) {

            for (int i = 0; i < text.Length; i++) {
                char symbol = text[i];
                bool isOperator = IsOperator(symbol);

                if ((isOperator || symbol == ')') && (i == 0) && symbol != '-') return false;
                if (isOperator && IsCharIndexCorrect(i + 1, text) && text[i + 1] == ')') return false;
                if ((isOperator || symbol == '(') && symbol != ')' && (i == text.Length - 1)) return false;
            }
            return true;
        }

        private static bool IsCharIndexCorrect(int index, string text) {

            return index >= 0 && index < text.Length;
        }

        private static string RemoveSpaces(string text) {

            string textWithOutSpaces = text.Replace(" ", "");
            return textWithOutSpaces;
        }

        private static bool IsDelimeter(char c) {
            return (" =".IndexOf(c) != -1);
        }

        private static bool IsOperator(char с) {
            return ("+-/*()".IndexOf(с) != -1);
        }

        private static byte GetPriority(char s) {
            switch (s) {
                case '(': return 0;
                case ')': return 1;
                case '+': return 2;
                case '-': return 3;
                case '*': return 4;
                case '/': return 4;
                default: return 6;
            }
        }

        private static bool HasUnacceptableSymbols(string text) {

            return Regex.IsMatch(text, @"[^()+-=/*\w]|[.]");
        }
    }
}
