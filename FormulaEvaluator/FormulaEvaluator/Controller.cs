﻿using System.Collections.Generic;
using System.Linq;

namespace FormulaEvaluator {
    class Controller {

        Model model = new Model();
        MainWindow mainWindow;
        SqlWorker sqlWorker = new SqlWorker();

        public Controller(MainWindow mw) {
            mainWindow = mw;

            if (!sqlWorker.CheckConnection()) {
                mainWindow.ShowError(Errors.ErrorConnection);
                return;
            }
            if (!sqlWorker.CheckTables()) {
                sqlWorker.CreateTable();
            }
            List<FormulaRawData> formulasRawData = sqlWorker.GetFormules();
            model.ParseFormules(formulasRawData);
            mainWindow.FillFormules(model.Formules.Select(x => x.Value).ToList());

            mainWindow.OnAddFormulaButtonClick += TryAddNewFormula;
            mainWindow.OnFormulaSelect += AddParameters;
            mainWindow.OnResultButtonClick += GetResult;
        }

        private void GetResult(Dictionary<string, double> variableValues, string formulaText) {
            
            if (variableValues == null || model.SelectedFormula == null)  return;

            string newLastParameters = "";
            foreach (var parameter in variableValues) {
                newLastParameters += $"{parameter.Key}={parameter.Value.ToString()};";
            }
            model.SelectedFormula = model.Formules.FirstOrDefault(x => x.Value.FormulaText == formulaText).Value;
            Formula formula = model.SelectedFormula;
            if (formula.LastParametersId != null) {
                sqlWorker.UpdateParameters(model.SelectedFormula.LastParametersId, newLastParameters);
                sqlWorker.UpdateFormula(model.SelectedFormula.FormulaId, model.SelectedFormula.FormulaText, model.SelectedFormula.LastParametersId);
            } else {
                int lastParametersId = sqlWorker.CreateParameters(newLastParameters);
                sqlWorker.UpdateFormula(model.SelectedFormula.FormulaId, model.SelectedFormula.FormulaText, lastParametersId);
                formula.LastParametersId = lastParametersId;
            }
            double result = formula.Command.Evaluate(variableValues);
            formula.VariableValues = variableValues;
            
            mainWindow.ShowResult(result.ToString()); 
        }

        private void TryAddNewFormula(string formula) {

            if (string.IsNullOrEmpty(formula)) {
                return;
            }
            if (model.Formules.Any(x => x.Value.FormulaText == formula)) {
                mainWindow.ShowError(Errors.FormulaAlreadyExists);
                return;
            }
            HashSet<string> parameters = new HashSet<string>();
            string formulaText;
            if (FormulaParser.CheckInputText(formula, out formulaText)) {
                parameters = FormulaParser.ParseParameters(formulaText);
                int formulaId = sqlWorker.WriteFormula(formulaText);
                Command command = FormulaParser.GetCommand(FormulaParser.GetExpression(formulaText));
                Formula newFormula = new Formula(formulaText, formulaId, null, command);
                model.Formules.Add(formulaId, newFormula);
                mainWindow.AddFormula(formulaText);
                mainWindow.SelectFormula(formulaText);
            } else {
                mainWindow.ShowError(Errors.FormulaSyntaxError);
                return;
            }
        }

        private void AddParameters(string formulaText) {
           
            model.SelectedFormula = model.Formules.FirstOrDefault(x => x.Value.FormulaText == formulaText).Value;
            Formula formula = model.SelectedFormula;

            if (formula != null && formula.LastParametersId != null) {
                mainWindow.ShowParameters(formula.VariableValues);
            } else {
                HashSet<string> parametersNames = FormulaParser.ParseParameters(formulaText);
                Dictionary<string, double> emptyParameters = new Dictionary<string, double>();
                foreach (var parametName in parametersNames) {
                    emptyParameters.Add(parametName, 0);
                }
                mainWindow.ShowParameters(emptyParameters);
            }
        }
    }
}
