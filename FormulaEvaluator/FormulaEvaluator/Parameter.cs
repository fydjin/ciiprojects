﻿using System.Windows.Controls;

namespace FormulaEvaluator {
    public class Parameter {

        public TextBox LastParameter { get; set; } = new TextBox();
        public Label ParameterLabel { get; set; } = new Label();
        public string Name { get; set; }

        public Parameter(string nameParameter, string lastParameter, string name) {
            
            LastParameter.Text = lastParameter;
            ParameterLabel.Target = LastParameter;
            ParameterLabel.Content = nameParameter;
            LastParameter.Name = name;
            Name = name;
        }
    }
}
