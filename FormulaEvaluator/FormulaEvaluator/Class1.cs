﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaEvaluator {
    class Class1 {

        public Command method(string reverseText) {

            Command command = null;
            Stack<Command> stack = new Stack<Command>();
            for (int i = reverseText.Length - 1; i >= 0; i--) {

                if (reverseText[i] == ' ') {
                    i--;
                }
                if (stack.Count == 0) {
                    command = CreateComm(reverseText[i].ToString());
                    stack.Push(command);
                } else {
                    if (stack.Peek().IsOperandsFull()) {
                        stack.Pop();
                    }
                    if (IsOperator(reverseText[i])) {
                        Command com = CreateComm(reverseText[i].ToString());
                        stack.Peek().AddOperand(com);
                        stack.Push(com);
                    } else {
                        if (Char.IsDigit(reverseText[i]) || reverseText[i] == ',') {
                            string value = string.Empty;
                            while (i > 0 && Char.IsDigit(reverseText[i]) || reverseText[i] == ',') {
                                value = reverseText[i] + value;
                                i--;
                            }
                            stack.Peek().AddOperand(new OneElemCom(Double.Parse(value)));
                        } else {
                            stack.Peek().AddOperand(new OneElemCom(reverseText[i].ToString()));
                        }
                    }
                }
            }
            return command;
        }

        private bool IsOperator(char с) {
            return ("+-/*".IndexOf(с) != -1);
        }

        private Command CreateComm(string operation) {

            switch (operation) {
                case "+":
                    return new AddCom();
                case "-":
                    return new SubCom();
                case "*":
                    return new MultCom();
                case "/":
                    return new DivCom();
            }
            return null;
        }
    }
}
