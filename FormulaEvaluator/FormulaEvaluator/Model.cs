﻿using System;
using System.Collections.Generic;

namespace FormulaEvaluator {
    class Model {

        public Dictionary<int, Formula> Formules { get; set; } = new Dictionary<int, Formula>();
        public Formula SelectedFormula { get; set; }

        public void ParseFormules(List<FormulaRawData> formulesFromDB) {

            foreach (var rawData in formulesFromDB) {
                Formula formula = FormulaParser.GetFormula(rawData);
                formula.VariableValues = ParseLastParameters(rawData.FormulaParams);
                Formules.Add(rawData.FormulaId, formula);
            }
        }

        public void ParseFormula(Formula formula) {

            formula.VariableValues = ParseLastParameters(formula.VariableValues.Values.ToString());
            Formules.Add(formula.FormulaId, formula);

        }

        public Dictionary<string, double> ParseLastParameters(string formulaParams) {

            Dictionary<string, double> parameters = new Dictionary<string, double>();
            string[] oneParameters = formulaParams.Split(';');
            if (String.IsNullOrEmpty(formulaParams)) {
                return parameters;
            }
            for (int i = 0; i < oneParameters.Length - 1; i++) {
                string[] parameterValuePair = oneParameters[i].Split('=');
                parameters.Add(parameterValuePair[0], Convert.ToDouble(parameterValuePair[1]));
            }
            return parameters;
        }
    }

    public enum Errors {
        ErrorConnection = 1,
        FormulaSyntaxError = 2,
        FormulaAlreadyExists = 3,
        InvalidParameterValue = 4
    }
}
