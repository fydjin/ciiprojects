﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaEvaluator {
    public abstract class Command {

        protected int requiredOperandNumber;
        protected List<Command> operands = new List<Command>();

        public abstract double Evaluate(Dictionary<string, double> parameters);
        public double Evaluate() {
            return Evaluate(new Dictionary<string, double>());
        }

        public bool IsOperandsFull() {

            return operands.Count >= requiredOperandNumber;
        }

        public void AddOperand(Command command) {

            operands.Add(command);
        }
    }
}
