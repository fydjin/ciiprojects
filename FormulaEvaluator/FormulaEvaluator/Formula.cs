﻿using System.Collections.Generic;

namespace FormulaEvaluator {
    public class Formula {

        public string FormulaText { get; set; }
        public int FormulaId { get; set; }
        public int? LastParametersId { get; set; }
        public Dictionary<string, double> VariableValues { get; set; }
        public Command Command;

        public Formula(string formulaText, int formulaId, int? lastParametersId, Command command) {
            FormulaText = formulaText;
            FormulaId = formulaId;
            LastParametersId = lastParametersId;
            Command = command;
        }
    }
}
