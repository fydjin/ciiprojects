﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;

namespace Client {
    class Connection {
        private TcpClient client;
        private NetworkStream stream;
        private MemoryStream memoryStream = new MemoryStream();
        Thread receiveThread;
        private const int port = 1777;
        private string IP;

        public event Action<MemoryStream> OnDataReceived;
        public event Action<string> OnListenException;
        public event Action<string> OnConnectionException;

        public Connection(string IP) {
            this.IP = IP;
        }

        public void OpenConnection() {
            client = new TcpClient();
            try {
                client.Connect(IP, port);
                stream = client.GetStream();
                receiveThread = new Thread(new ThreadStart(ListenNewData));
                receiveThread.Start();
            } catch (Exception ex) {
                OnConnectionException?.Invoke(ex.ToString());
            }
        }

        public void Write(byte[] messagePacket) {
            stream.Write(messagePacket, 0, messagePacket.Length);
        }

        private void ListenNewData() {
            while (true) {
                try {
                    byte[] data = new byte[64];
                    int bytes = 0;
                    do {
                        bytes = stream.Read(data, 0, data.Length);
                        memoryStream.Write(data, 0, data.Length);
                    }
                    while (stream.DataAvailable);
                    stream.Flush();
                    memoryStream.Seek(0, SeekOrigin.Begin);
                    OnDataReceived?.Invoke(memoryStream);
                    memoryStream.SetLength(0);

                } catch (Exception ex) {
                    OnListenException?.Invoke(ex.ToString());
                }
            }
        }

        public void Disconnect() {
            receiveThread.Abort();
            stream?.Close();
            client?.Close();
        }
    }
}

