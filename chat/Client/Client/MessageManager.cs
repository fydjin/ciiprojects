﻿using ChatLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ChatLibrary.SerializeWorker;

namespace Client
{
    class MessageManager
    {
        public event Action<List<string>> OnLogAnswer;
        public event Action<UserMessage> OnMessage;

        public void ParseMessageData(MessagePacket messagePacket)
        {
            switch (messagePacket.Type)
            {
                case MessageType.Message:
                    UserMessage userMessage = (UserMessage)Deserialize(messagePacket.Data);
                    OnMessage?.Invoke(userMessage);
                    break;
                case MessageType.Log:
                    List<string> log = (List<string>)Deserialize(messagePacket.Data);
                    OnLogAnswer?.Invoke(log);
                    break;
            }
        }

        public MessagePacket CreateMessagePacket(MessageType type, object messageData)
        {
            return new MessagePacket(type, Serialize(messageData));
        }
    }
}
