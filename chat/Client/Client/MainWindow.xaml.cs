﻿using ChatLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;

namespace Client {
    public partial class MainWindow : Window {

        private const string messageFormat = "{0}: {1}";
        private const string logFormat = "Log: {0}";

        public event Action<string> OnSendClick;
        public event Action OnWindowClose;
        public event Action OnGetLogClick;

        public MainWindow() {
            InitializeComponent();
            this.Closing += CloseWindow;
            new Controller(this);
        }

        private void CloseWindow(object sender, CancelEventArgs e) {
            OnWindowClose?.Invoke();
        }

        public void ShowLog(List<string> Log) {

            ListBoxWithMessages.Dispatcher.Invoke(() => {
                foreach (var message in Log) {

                    ListBoxWithMessages.Items.Add(String.Format(logFormat, message));
                }
            });
        }

        private void OnSendButtonClick(object sender, RoutedEventArgs e) {
            string message = TextBoxForEntryMessage.Text;
            if (!String.IsNullOrEmpty(message)) {
                OnSendClick?.Invoke(message);
            }
        }

        public void ClearTextBoxForEntry() {
            TextBoxForEntryMessage.Clear();
        }

        private void OnGetLogButtonClick(object sender, RoutedEventArgs e) {

            OnGetLogClick?.Invoke();
        }

        public void AddNewMessageInList(UserMessage message) {
            ListBoxWithMessages.Items.Add(String.Format(messageFormat, message.Name, message.Message));
        }
    }
}
