﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using ChatLibrary;

namespace Client {
    class Controller {
        private MainWindow mainWindow;
        private AutorizationWindow autorizationWindow;
        private ConnectionWrapper connectionWrapper;
        private MessageManager messageManager = new MessageManager();
        private string name;

        public Controller(MainWindow mw) {
            mainWindow = mw;
            mainWindow.OnSendClick += SendMessage;
            mainWindow.OnSendClick += InitNewMessage;
            mainWindow.OnGetLogClick += RequestLog;
            messageManager.OnMessage += HandleMessage;
            messageManager.OnLogAnswer += HandleLog;
            ShowAutorizationWindow(GetLocalIP());
        }

        private void HandleLog(List<string> log) {
            mainWindow.ShowLog(log);
        }

        private void Initialization(string name, string ip) {
            this.name = name;
            connectionWrapper = new ConnectionWrapper(ip);
            connectionWrapper.OpenConnection();
            connectionWrapper.OnNewMessagePacket += HandleMessage;
            connectionWrapper.OnListenException += HandleError;
            connectionWrapper.OnConnectionException += ConnectionError;
            mainWindow.OnWindowClose += connectionWrapper.Disconnect;

            MessagePacket messagePacket = messageManager.CreateMessagePacket(MessageType.Name, name);
            connectionWrapper.SendPacket(messagePacket);

            autorizationWindow.Hide();
            mainWindow.Show();
        }

        private void HandleError(string ex) {
            connectionWrapper.Disconnect();
            MessageBox.Show(ex);
        }

        private void ConnectionError(string ex) {
            connectionWrapper.Disconnect();
            MessageBox.Show(ex);
        }

        private void HandleMessage(UserMessage message) {
            mainWindow.AddNewMessageInList(message);
        }

        private string GetLocalIP() {
            IPAddress[] IP = Dns.GetHostAddresses(Dns.GetHostName());

            return IP[1].ToString();
        }

        private void RequestLog() {
            MessagePacket messagePacket = new MessagePacket(MessageType.Log);
            connectionWrapper.SendPacket(messagePacket);
        }

        private void InitNewMessage(string message) {
            mainWindow.ClearTextBoxForEntry();
            mainWindow.AddNewMessageInList(new UserMessage(name, message));
        }

        private void HandleMessage(MessagePacket messagePacket) {
            messageManager.ParseMessageData(messagePacket);
        }

        private void SendMessage(string message) {
            MessagePacket messagePacket = messageManager.CreateMessagePacket(MessageType.Message, message);
            connectionWrapper.SendPacket(messagePacket);
        }

        private void ShowAutorizationWindow(string ip) {
            mainWindow.Hide();
            autorizationWindow = new AutorizationWindow();
            autorizationWindow.AddIPInTextBox(ip);
            autorizationWindow.OnConnectionClick += Initialization;
            autorizationWindow.Show();
        }

    }
}
