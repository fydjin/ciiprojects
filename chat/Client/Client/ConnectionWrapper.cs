﻿using ChatLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static ChatLibrary.SerializeWorker;

namespace Client
{
    class ConnectionWrapper
    {
        private Connection connection;
        private BinaryFormatter formatter = new BinaryFormatter();

        public event Action<MessagePacket> OnNewMessagePacket;
        public event Action<string> OnListenException;
        public event Action<string> OnConnectionException;

        public ConnectionWrapper(string ip)
        {
            connection = new Connection(ip);
            connection.OnDataReceived += DeserializeMessagePacket;
            connection.OnListenException += ListenException; 
            connection.OnConnectionException += ConnectionException;
        }

        private void ConnectionException(string ex) {
            OnConnectionException.Invoke(ex);
        }

        public void Disconnect()
        {
            connection.Disconnect();
            connection.OnDataReceived -= DeserializeMessagePacket;
            connection.OnListenException -= ListenException;
            connection.OnConnectionException -= ConnectionException;
        }

        public void OpenConnection()
        {
            connection.OpenConnection();
        }

        private void ListenException(string ex)
        {
            OnListenException?.Invoke(ex);
        }

        public void SendPacket(MessagePacket messagePacket)
        {
            connection.Write(Serialize(messagePacket));
        }

        private void DeserializeMessagePacket(MemoryStream memoryStream)
        {
            MessagePacket messagePacket = (MessagePacket)formatter.Deserialize(memoryStream);
            OnNewMessagePacket?.Invoke(messagePacket);
        }
    }
}
