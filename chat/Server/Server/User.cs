﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ChatLibrary;

namespace Server {
    class User {

        public string Id;
        public string Name;
        private ConnectionWrapper connectionWrapper;
        private TcpClient tcpClient;
        private Thread clientThread;


        public event Action<MessagePacket, User> OnMessagePacketReceived;
        public event Action<string, User> OnListenException;

        public User(NetworkStream stream, TcpClient tcpClient) {
            Id = Guid.NewGuid().ToString();
            this.tcpClient = tcpClient;
            connectionWrapper = new ConnectionWrapper(stream);
            connectionWrapper.OnNewMessagePacket += MessagePacketReceive;
            connectionWrapper.OnListenException += ListenException;

            clientThread = new Thread(new ThreadStart(ListenNewData));
            clientThread.Start();
        }

        private void ListenException(string ex) {
            OnListenException?.Invoke(ex, this);
        }

        public void ListenNewData() {
            connectionWrapper.ListenNewData();
        }

        public void SendPacket(MessagePacket packet) {
            connectionWrapper.SendPacket(SerializeWorker.Serialize(packet));
        }

        private void MessagePacketReceive(MessagePacket messagePacket) {
            OnMessagePacketReceived?.Invoke(messagePacket, this);
        }

        public void Close() {
            tcpClient.Close();
            clientThread.Abort();
        }
    }
}
