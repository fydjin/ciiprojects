﻿using System;
using System.IO;
using System.Net.Sockets;

namespace Server {
    class Connection {
        private NetworkStream stream;
        private MemoryStream memoryStream = new MemoryStream();

        public event Action<MemoryStream> OnDataReceived;
        public event Action<string> OnListenException;

        public Connection(NetworkStream stream) {
            this.stream = stream;
        }

        public void ListenNewData() {
            while (true) {
                try {
                    do {
                        byte[] data = new byte[64];
                        int bytes = 0;
                        bytes = stream.Read(data, 0, data.Length);
                        memoryStream.Write(data, 0, data.Length);
                    }
                    while (stream.DataAvailable);
                    stream.Flush();
                    memoryStream.Seek(0, SeekOrigin.Begin);
                    OnDataReceived?.Invoke(memoryStream);
                    memoryStream.SetLength(0);
                } catch (Exception ex) {
                    OnListenException?.Invoke(ex.ToString());
                }
            }
        }

        public void Write(byte[] messagePacket) {
            stream.Write(messagePacket, 0, messagePacket.Length);
        }
    }
}
