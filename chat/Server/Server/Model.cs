﻿using ChatLibrary;
using System;
using System.Collections.Generic;

namespace Server
{
    class Model
    {
        public string Path;
        private MaxHeap logHeap = new MaxHeap();
        private List<UserMessage> log = new List<UserMessage>();
        object _lock = new object();

        public void AddToLog(string name, string message)
        {
            lock (_lock)
            {
                logHeap.Add(new UserMessage(name, message));
            }
        }

        public void AddFileLinesInLog(List<string> fileLines)
        {
            if (fileLines.Count > 0)
            {
                InitializationLogHeap(ParseFileLines(fileLines));
            }
        }

        public List<UserMessage> GetLog()
        {
            lock (_lock)
            {
                log = logHeap.GetSortHeap();
            }
            return log;
        }

        private void InitializationLogHeap(List<UserMessage> messages)
        {
            foreach (var line in messages)
            {
                lock (_lock)
                {
                    logHeap.Add(line);
                }
            }
        }

        private List<UserMessage> ParseFileLines(List<string> fileLines)
        {
            List<UserMessage> messages = new List<UserMessage>();
            foreach (var line in fileLines)
            {
                messages.Add(SplitLine(line));
            }
            return messages;
        }

        private UserMessage SplitLine(string line)
        {
            UserMessage userMessage;
            string message = String.Empty;
            string name = String.Empty;
            for (int i = 0; i < line.Length; i++)
            {
                if (line[i] == ':')
                {
                    name = line.Remove(i, line.Length - 1 - i);
                    message = line.Remove(0, i + 1);
                    break;
                }
            }
            userMessage = new UserMessage(name, message);
            return userMessage;
        }
    }
}
