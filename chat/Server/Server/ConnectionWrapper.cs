﻿using ChatLibrary;
using System;
using System.IO;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;

namespace Server {
    class ConnectionWrapper {
        private Connection connection;
        private BinaryFormatter formatter = new BinaryFormatter();

        public event Action<MessagePacket> OnNewMessagePacket;
        public event Action<string> OnListenException;

        public ConnectionWrapper(NetworkStream stream) {
            connection = new Connection(stream);
            connection.OnDataReceived += DeserializeMessagePacket;
            connection.OnListenException += ListenException;
        }

        public void ListenNewData() {
            connection.ListenNewData();
        }

        private void ListenException(string ex) {
            OnListenException?.Invoke(ex);
        }

        public void SendPacket(byte[] messagePacket) {
            connection.Write(messagePacket);
        }

        private void DeserializeMessagePacket(MemoryStream memoryStream) {
            MessagePacket messagePacket = (MessagePacket)formatter.Deserialize(memoryStream);
            OnNewMessagePacket?.Invoke(messagePacket);
        }
    }
}
