﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using ChatLibrary;

namespace Server {

    class Server {

        private Thread listenThread;
        private TcpListener tcpListener;
        public HashSet<User> users = new HashSet<User>();
        private NetworkStream stream;

        public event Action<MessagePacket, User> OnMessagePacketReceived;

        public void StartServer()
        {
            listenThread = new Thread(new ThreadStart(ListenNewUser));
            listenThread.Start();
        }

        public void RemoveConnection(string id) {
            User client = users.FirstOrDefault(c => c.Id == id);
            if (client != null) {
                users.Remove(client);
            }
        }

        public void ListenNewUser() {
            try {
                tcpListener = new TcpListener(IPAddress.Any, 1777);
                tcpListener.Start();
                Console.WriteLine("Сервер запущен. Ожидание подключений...");

                while (true) {
                    TcpClient tcpClient = tcpListener.AcceptTcpClient();
                    AddNewUser(tcpClient);
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                Disconnect();
            }
        }

        private void AddNewUser(TcpClient tcpClient)
        {
            stream = tcpClient.GetStream();
            User user = new User(stream, tcpClient);
            users.Add(user);
            user.OnMessagePacketReceived += MessagePacketReceived;
            user.OnListenException += HandleException;
        }

        private void DisconnectUser(User user) {
            user.Close();
            users.Remove(user);
            user.OnMessagePacketReceived -= MessagePacketReceived;
            user.OnListenException -= HandleException;
        }

        private void HandleException(string ex, User user)
        {
            DisconnectUser(user);
            Console.WriteLine(ex);
        }

        private void MessagePacketReceived(MessagePacket messagePacket, User user) {
            OnMessagePacketReceived?.Invoke(messagePacket, user);
        }

        public void BroadcastMessage(User userSender, MessagePacket messagePacket) {
            foreach (var user in users) {
                if (userSender.Id != user.Id) {
                    user.SendPacket(messagePacket);
                }
            }
        }

        protected internal void Disconnect() {

            tcpListener.Stop();
            foreach (var user in users) {
                user.Close();
            }
            users.Clear();
        }
    }
}
