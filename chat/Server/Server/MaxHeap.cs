﻿using ChatLibrary;
using System;
using System.Collections.Generic;

namespace Server {
    class MaxHeap {
        private List<UserMessage> messages = new List<UserMessage>();
        private List<UserMessage> tempMessages;
        private int size;
        private int tempSize;

        public List<UserMessage> GetSortHeap() {
            List<UserMessage> sortMessages = new List<UserMessage>();
            tempMessages = new List<UserMessage>(messages);
            tempSize = size;
            for (int i = 0; i < tempMessages.Count; i++) {
                sortMessages.Add(Pop());
            }

            return sortMessages;
        }

        private UserMessage Pop() {
            UserMessage result = tempMessages[0];
            tempMessages[0] = tempMessages[tempSize - 1];
            tempSize--;

            ReCalculateDown();

            return result;
        }

        private UserMessage GetParent(int elementIndex) {
            return messages[(elementIndex - 1) / 2];
        }

        public void Add(UserMessage element) {
            messages.Add(element);
            size++;
            Refresh();
        }

        private bool IsRoot(int elementIndex) {
            return elementIndex == 0;
        }

        private void Swap(int firstIndex, int secondIndex) {
            var temp = messages[firstIndex];
            messages[firstIndex] = messages[secondIndex];
            messages[secondIndex] = temp;
        }

        private void SwapTempMessages(int firstIndex, int secondIndex)
        {
            var temp = tempMessages[firstIndex];
            tempMessages[firstIndex] = tempMessages[secondIndex];
            tempMessages[secondIndex] = temp;
        }

        private void Refresh() {
            var index = size - 1;

            while (!IsRoot(index) && String.Compare(messages[index].Message, GetParent(index).Message) < 0) {
                var parentIndex = (index - 1) / 2;
                Swap(parentIndex, index);
                index = parentIndex;
            }
        }

        private void ReCalculateDown() {
            int index = 0;
            while (HasLeftChild(index)) {
                var biggerIndex = 2 * index + 1;
                if (HasRightChild(index) && String.Compare(GetRightChild(index).Message, GetLeftChild(index).Message) < 0) {
                    biggerIndex = GetRightChildIndex(index);
                }

                if (String.Compare(tempMessages[biggerIndex].Message, tempMessages[index].Message) > 0) {
                    break;
                }

                SwapTempMessages(biggerIndex, index);
                index = biggerIndex;
            }
        }

        private bool HasLeftChild(int elementIndex) {
            return elementIndex * 2 + 1 < tempSize;
        }

        private UserMessage GetLeftChild(int elementIndex) {
            return tempMessages[elementIndex * 2 + 1];
        }

        private int GetRightChildIndex(int elementIndex) {
            return 2 * elementIndex + 2;
        }

        private bool HasRightChild(int elementIndex) {
            return elementIndex * 2 + 2 < tempSize;
        }

        private UserMessage GetRightChild(int elementIndex) {
            return tempMessages[elementIndex * 2 + 2];
        }
    }
}
