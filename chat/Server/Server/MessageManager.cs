﻿using ChatLibrary;
using System;
using System.Collections.Generic;
using static ChatLibrary.SerializeWorker;

namespace Server {
    class MessageManager {
        public event Action<User> OnLogRequest;
        public event Action<User, string> OnMessage;
        public event Action<User, string> OnAuthorization;
        private const string messageFormat = "{0}: {1}";

        public void ParseMessageData(MessagePacket messagePacket, User user) {
            switch (messagePacket.Type) {
                case MessageType.Message:
                    var message = (string)Deserialize(messagePacket.Data);
                    OnMessage?.Invoke(user, message);
                    break;
                case MessageType.Log:
                    OnLogRequest?.Invoke(user);
                    break;
                case MessageType.Name:
                    var name = (string)Deserialize(messagePacket.Data);
                    OnAuthorization?.Invoke(user, name);
                    break;
            }
        }

        public MessagePacket CreateMessagePacket(MessageType type, object messageData) {
            return new MessagePacket(type, Serialize(messageData));
        }

        public List<string> ParseUserMessageInLine(List<UserMessage> userMessages) {
            List<string> log = new List<string>();
            foreach (var userMessage in userMessages)
            {
                log.Add(String.Format(messageFormat, userMessage.Name, userMessage.Message));
            }
            return log;
        }
    }
}
