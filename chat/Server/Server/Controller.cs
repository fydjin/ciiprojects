﻿using System;
using System.Threading;
using System.Threading.Tasks;
using ChatLibrary;

namespace Server
{
    class Controller
    {
        private FilesWorker filesWorker = new FilesWorker();
        private Model model = new Model();
        private Server server = new Server();
        private MessageManager messageManager = new MessageManager();

        public void Initialization()
        {
            model.Path = filesWorker.GetPathDataFile();
            messageManager.OnMessage += HandleMessage;
            messageManager.OnLogRequest += HandleLog;
            messageManager.OnAuthorization += HandleAuthorization;
            model.AddFileLinesInLog(filesWorker.ReadFile(model.Path));
            server.StartServer();
            server.OnMessagePacketReceived += HandleMessagePacket;
        }

        private void HandleAuthorization(User user, string name)
        {
            user.Name = name;
        }

        private void HandleLog(User user)
        {
            MessagePacket messagePacket = messageManager.CreateMessagePacket(
                MessageType.Log, messageManager.ParseUserMessageInLine(model.GetLog()));
            user.SendPacket(messagePacket);
        }

        private void HandleMessage(User user, string message)
        {
            model.AddToLog(user.Name, message);
            UserMessage userMessage = new UserMessage(user.Name, message);
            MessagePacket messagePacket = messageManager.CreateMessagePacket(MessageType.Message, userMessage);
            server.BroadcastMessage(user, messagePacket);
            WriteInFile();
        }

        private void HandleMessagePacket(MessagePacket messageType, User user)
        {
            messageManager.ParseMessageData(messageType, user);
        }

        private void WriteInFile()
        {
            filesWorker.WriteLogInFile(messageManager.ParseUserMessageInLine(model.GetLog()), model.Path);
        }
    }
}
