﻿using ChatLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class FilesWorker
    {
        private const string FileName = "data.txt";
        private object _lock = new object();

        public string GetPathDataFile()
        {
            string path = Environment.CurrentDirectory;
            if (!File.Exists(FileName))
            {
                using (FileStream fs = new FileStream(path, FileMode.Create))
                {
                    File.Create($@"{path}\{FileName}");
                }
            }
            path = $@"{path}\{FileName}";
            return path;
        }

        public List<string> ReadFile(string path)
        {
            Task<List<string>> task = new Task<List<string>>(() => Read(path));
            task.Start();
            return task.Result;
        }

        public async void WriteLogInFile(List<string> log, string path)
        {
            List<string> tempLog;
            if (log.Count > 0)
            {
                lock (_lock)
                {
                    tempLog = new List<string>(log);
                }
                await Task.Run(() => Write(tempLog, path));
            }
        }

        private async Task Write(List<string> messagesForRecording, string path)
        {
            using (StreamWriter sw = new StreamWriter(path, false, Encoding.Default))
            {
                foreach (var line in messagesForRecording)
                {
                    await sw.WriteLineAsync(line);
                }
            }
        }

        private List<string> Read(string path)
        {
            List<string> log = new List<string>();
            using (StreamReader streamReader = new StreamReader(path, Encoding.Default))
            {
                string line;
                while ((line = streamReader.ReadLine()) != null)
                {
                    log.Add(line);
                }
            }
            return log;
        }
    }
}
