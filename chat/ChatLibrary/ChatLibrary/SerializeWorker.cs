﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace ChatLibrary {
    public static class SerializeWorker {
        private static BinaryFormatter formatter = new BinaryFormatter();
        private static MemoryStream memoryStream = new MemoryStream();

        public static byte[] Serialize(object obj) {
            memoryStream.SetLength(0);
            formatter.Serialize(memoryStream, obj);
            byte[] data = memoryStream.ToArray();
            return data;
        }

        public static object Deserialize(byte[] data) {
            memoryStream.SetLength(0);
            memoryStream.Write(data, 0, data.Length);
            memoryStream.Seek(0, SeekOrigin.Begin);
            return formatter.Deserialize(memoryStream);
        }
    }
}
