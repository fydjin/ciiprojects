﻿using System;

namespace ChatLibrary {

    [Serializable]
    public class MessagePacket {
        public MessageType Type { get; set; }
        public byte[] Data { get; set; }

        public MessagePacket(MessageType type, byte[] data = null) {
            Type = type;
            Data = data;
        }
    }
}


public enum MessageType {
    Message,
    Log,
    Name
}