﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatLibrary
{
    [Serializable]
   public class UserMessage
    {
        public string Name;
        public string Message;

        public UserMessage(string name, string message)
        {
            Name = name;
            Message = message;
        }
    }
}
